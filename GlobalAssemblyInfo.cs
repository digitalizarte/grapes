using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Brandy")]
[assembly: AssemblyProduct("Brandy.Grapes")]
[assembly: AssemblyCopyright("Copyright � \"BrandyFx\" 2012 - 2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]